﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

enum PackageNodeType
{
    Root,
    Package,
    PackageOfInterest,
    Dependencies,
    Requires,
    Version
}

public class Program
{

    public static void Main(string[] args)
    {
        AppDomain.MonitoringIsEnabled = true;

        foreach (string arg in args) 
        {
            SimpleFindVersion(arg);
        }

        double totalMilliseconds = AppDomain.CurrentDomain.MonitoringTotalProcessorTime.TotalMilliseconds;
        long monitoringTotalAllocatedMemorySize = AppDomain.CurrentDomain.MonitoringTotalAllocatedMemorySize;
        Console.WriteLine($"Took: {totalMilliseconds:#,###} ms");
        Console.WriteLine($"Allocated: {monitoringTotalAllocatedMemorySize / 1024:#,#} kb");
        
        for (var index = 0; index <= GC.MaxGeneration; index++)
        {
            Console.WriteLine($"Gen {index} collections: {GC.CollectionCount(index)}");
        }
    }

    public static void SimpleFindVersion(string package)
    {
        JObject jsonDoc = JObject.Parse(File.ReadAllText("package-lock.json"));

        string version = jsonDoc.SelectTokens("$.." + package + ".version", false).First().ToString();

        Console.WriteLine(version);
    }

    public static void SophisticatedFindVersion(string package)
    {

        using (Stream stream = File.OpenRead("package-lock.json"))
        using (StreamReader reader = new StreamReader(stream))
        using (JsonTextReader jsonReader = new JsonTextReader(reader))
        {
            
            PackageNodeType currentNodeType = PackageNodeType.Root;

            while (jsonReader.Read())
            {
                switch (currentNodeType)
                {
                    case PackageNodeType.Root:
                        if (jsonReader.TokenType == JsonToken.PropertyName && string.Equals(jsonReader.Value, "dependencies"))
                        {
                            currentNodeType = PackageNodeType.Dependencies;
                        }
                        break;
                    case PackageNodeType.Dependencies:
                        if (jsonReader.TokenType == JsonToken.PropertyName)
                        {
                            if (string.Equals(jsonReader.Value, package))
                            {
                                currentNodeType = PackageNodeType.PackageOfInterest;
                            }
                            else
                            {
                                currentNodeType = PackageNodeType.Package;
                            }
                        }
                        else if (jsonReader.TokenType == JsonToken.EndObject)
                        {
                            currentNodeType = PackageNodeType.Package;
                        }
                        break;
                    case PackageNodeType.Requires:
                        if (jsonReader.TokenType == JsonToken.EndObject)
                        {
                            currentNodeType = PackageNodeType.Package;
                        }
                        break;
                    case PackageNodeType.Package:
                    case PackageNodeType.PackageOfInterest:
                        if (jsonReader.TokenType == JsonToken.PropertyName && string.Equals(jsonReader.Value, "dependencies"))
                        {
                            currentNodeType = PackageNodeType.Dependencies;
                        }
                        else if (jsonReader.TokenType == JsonToken.PropertyName && string.Equals(jsonReader.Value, "requires"))
                        {
                            currentNodeType = PackageNodeType.Requires;
                        }
                        else if (currentNodeType == PackageNodeType.PackageOfInterest && jsonReader.TokenType == JsonToken.PropertyName && string.Equals(jsonReader.Value, "version"))
                        {
                            currentNodeType = PackageNodeType.Version;
                        }
                        else if (jsonReader.TokenType == JsonToken.EndObject)
                        {
                            currentNodeType = PackageNodeType.Dependencies;
                        }
                        break;
                    case PackageNodeType.Version:
                        if (jsonReader.TokenType == JsonToken.String)
                        {
                            Console.WriteLine(jsonReader.Value);
                            currentNodeType = PackageNodeType.Package;
                        }
                        break;
                }

            }
            
        }
        
    }

}