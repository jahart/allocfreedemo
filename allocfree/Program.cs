using System;
using System.Buffers;
using System.Diagnostics;
using System.IO;
using System.Text.Json;

enum PackageNodeType
{
    Root,
    Package,
    PackageOfInterest,
    Dependencies,
    Requires,
    Version
}

public class Program
{

    public static void Main(string[] args)
    {
        AppDomain.MonitoringIsEnabled = true;

        foreach (string arg in args) 
        {
            FindVersion(arg);
        }
        
        double totalMilliseconds = AppDomain.CurrentDomain.MonitoringTotalProcessorTime.TotalMilliseconds;
        long monitoringTotalAllocatedMemorySize = AppDomain.CurrentDomain.MonitoringTotalAllocatedMemorySize;
        Console.WriteLine($"Took: {totalMilliseconds:#,###} ms");
        Console.WriteLine($"Allocated: {monitoringTotalAllocatedMemorySize / 1024:#,#} kb");

        for (var index = 0; index <= GC.MaxGeneration; index++)
        {
            Console.WriteLine($"Gen {index} collections: {GC.CollectionCount(index)}");
        }
    }

    static void FindVersion(string package)
    {
        IMemoryOwner<byte> buffer = MemoryPool<byte>.Shared.Rent(1024);

        Span<char> charBuffer = stackalloc char[128];

        using (Stream stream = File.OpenRead("package-lock.json"))
        {
            Utf8JsonReader jsonReader = default;
            int numberOfBytesInBuffer = -1;

            PackageNodeType currentNodeType = PackageNodeType.Root;

            while (true)
            {
                bool eof = false;

                if (!jsonReader.Read())
                {
                    if (numberOfBytesInBuffer == 0)
                    {
                        eof = true;
                        break;
                    }

                    int leftOver = 0;

                    if (numberOfBytesInBuffer != -1)
                    {
                        leftOver = numberOfBytesInBuffer - (int)jsonReader.BytesConsumed;

                        if (leftOver != 0)
                        {
                            buffer.Memory.Slice(numberOfBytesInBuffer - leftOver, leftOver).CopyTo(buffer.Memory);
                        }
                    }

                    numberOfBytesInBuffer = leftOver + stream.Read(buffer.Memory[leftOver..].Span);
                    jsonReader = new Utf8JsonReader(buffer.Memory[0..numberOfBytesInBuffer].Span, 
                            numberOfBytesInBuffer == 0, jsonReader.CurrentState);
                }

                if (eof) break;

                switch (currentNodeType)
                {
                    case PackageNodeType.Root:
                        if (jsonReader.TokenType == JsonTokenType.PropertyName && jsonReader.ValueTextEquals("dependencies"))
                        {
                            currentNodeType = PackageNodeType.Dependencies;
                        }
                        break;
                    case PackageNodeType.Dependencies:
                        if (jsonReader.TokenType == JsonTokenType.PropertyName)
                        {
                            if (jsonReader.ValueTextEquals(package))
                            {
                                currentNodeType = PackageNodeType.PackageOfInterest;
                            }
                            else
                            {
                                currentNodeType = PackageNodeType.Package;
                            }
                        }
                        else if (jsonReader.TokenType == JsonTokenType.EndObject)
                        {
                            currentNodeType = PackageNodeType.Package;
                        }
                        break;
                    case PackageNodeType.Requires:
                        if (jsonReader.TokenType == JsonTokenType.EndObject)
                        {
                            currentNodeType = PackageNodeType.Package;
                        }
                        break;
                    case PackageNodeType.Package:
                    case PackageNodeType.PackageOfInterest:
                        if (jsonReader.TokenType == JsonTokenType.PropertyName && jsonReader.ValueTextEquals("dependencies"))
                        {
                            currentNodeType = PackageNodeType.Dependencies;
                        }
                        else if (jsonReader.TokenType == JsonTokenType.PropertyName && jsonReader.ValueTextEquals("requires"))
                        {
                            currentNodeType = PackageNodeType.Requires;
                        }
                        else if (currentNodeType == PackageNodeType.PackageOfInterest && jsonReader.TokenType == JsonTokenType.PropertyName && jsonReader.ValueTextEquals("version"))
                        {
                            currentNodeType = PackageNodeType.Version;
                        }
                        else if (jsonReader.TokenType == JsonTokenType.EndObject)
                        {
                            currentNodeType = PackageNodeType.Dependencies;
                        }
                        break;
                    case PackageNodeType.Version:
                        if (jsonReader.TokenType == JsonTokenType.String)
                        {
                            int chars = System.Text.Encoding.UTF8.GetChars(jsonReader.ValueSpan, charBuffer);
                            for(int i = 0; i < chars; i++)
                            {
                                Console.Write(charBuffer[i]);
                            }
                            Console.WriteLine();
                            currentNodeType = PackageNodeType.Package;
                        }
                        break;
                }

            }
            
        }
        
    }

}